﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenAirLibrary;

namespace OpenAirLibrary
{
    [TestFixture]
    class OpenAirTest
    {
        [Test]
        public void 註冊會員()
        {
            OpenAir system = new OpenAir();
            MemberRecord user = new MemberRecord();

            Assert.That(user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan")
                        , Is.EqualTo(true));
            user = user.Login("ABC", "a123");
            Assert.That(user.GetID(), Is.EqualTo("ABC"));
            Assert.That(user.GetPassword(), Is.EqualTo("a123"));
            Assert.That(user.GetName(), Is.EqualTo("Joe"));
            Assert.That(user.GetPhone(), Is.EqualTo("0123456789"));
            Assert.That(user.GetEmail(), Is.EqualTo("Joe@gmail.com"));
            Assert.That(user.GetAddress(), Is.EqualTo("Taiwan"));

            system.Close();
        }

        [Test]
        public void 註冊一個以上會員()
        {
            OpenAir system = new OpenAir();
            MemberRecord user1 = new MemberRecord();
            Assert.That(user1.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan")
                       , Is.EqualTo(true));

            user1 = user1.Login("ABC", "a123");
            Assert.That(user1.GetID(), Is.EqualTo("ABC"));
            Assert.That(user1.GetPassword(), Is.EqualTo("a123"));
            Assert.That(user1.GetName(), Is.EqualTo("Joe"));
            Assert.That(user1.GetPhone(), Is.EqualTo("0123456789"));
            Assert.That(user1.GetEmail(), Is.EqualTo("Joe@gmail.com"));
            Assert.That(user1.GetAddress(), Is.EqualTo("Taiwan"));

            MemberRecord user2 = new MemberRecord();
            Assert.That(user2.Register("abc", "A123", "David", "0987654321", "David@gmail.com", "Japan")
                       , Is.EqualTo(true));

            user2 = user2.Login("abc", "A123");
            Assert.That(user2.GetID(), Is.EqualTo("abc"));
            Assert.That(user2.GetPassword(), Is.EqualTo("A123"));
            Assert.That(user2.GetName(), Is.EqualTo("David"));
            Assert.That(user2.GetPhone(), Is.EqualTo("0987654321"));
            Assert.That(user2.GetEmail(), Is.EqualTo("David@gmail.com"));
            Assert.That(user2.GetAddress(), Is.EqualTo("Japan"));


            system.Close();
        }

        [Test]
        public void 註冊空白()
        {
            OpenAir system = new OpenAir();
            MemberRecord user = new MemberRecord();
            Assert.That(user.Register("      ", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan")
                       , Is.EqualTo(false));

            system.Close();
        }

        [Test]
        public void 註冊已存在帳號()
        {
            OpenAir system = new OpenAir();
            MemberRecord user1 = new MemberRecord();
            Assert.That(user1.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan")
                        , Is.EqualTo(true));

            MemberRecord user2 = new MemberRecord();
            Assert.That(user2.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan")
                        , Is.EqualTo(false));

            system.Close();
        }

        [Test]
        public void 正確登入()
        {
            OpenAir system = new OpenAir();
            MemberRecord user = new MemberRecord();
            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            Assert.That(user.Login("ABC", "a123"), Is.EqualTo(system.GetAccount("ABC", "a123")));

            system.Close();
        }

        [Test]
        public void 登入錯誤()
        {
            OpenAir system = new OpenAir();
            MemberRecord user = new MemberRecord();
            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            Assert.That(user.Login("ABC", "a321"), Is.EqualTo(null));

            system.Close();
        }

        [Test]
        public void 統計登入及登出()
        {
            OpenAir system = new OpenAir();
         
            MemberRecord user1 = new MemberRecord();
            Assert.That(user1.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan")
                       , Is.EqualTo(true));

            user1 = user1.Login("ABC", "a123");

            MemberRecord user2 = new MemberRecord();
            Assert.That(user2.Register("abc", "A123", "David", "0987654321", "David@gmail.com", "Japan")
                       , Is.EqualTo(true));

            user2 = user2.Login("ABC", "a123");
            Assert.That(system.GetMemberLoginTotalCount(), Is.EqualTo(2));

            user1.Logout();
            Assert.That(system.GetMemberLoginTotalCount(), Is.EqualTo(1));

            system.Close();

            Assert.That(system.GetMemberLoginTotalCount(), Is.EqualTo(0));

        }

        [Test]
        public void 修改個人資料()
        {
            OpenAir system = new OpenAir();

            MemberRecord user1 = new MemberRecord();
            Assert.That(user1.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan")
                       , Is.EqualTo(true));
            user1 = user1.Login("ABC", "a123");


            Assert.That(user1.ModifyCheckPassword("321a"), Is.EqualTo("密碼錯誤,請再重新輸入!!"));
            Assert.That(user1.ModifyCheckPassword("a123"), Is.EqualTo("true"));

            user1.Modify("321a", "Alex", "0123456789", "Alex@gmail.com", "Taiwan");
            Assert.That(user1.GetName(), Is.EqualTo("Alex"));
            Assert.That(user1.GetPassword(), Is.EqualTo("321a"));

            system.Close();
        }

        [Test]
        public void 增加商品()
        {
            OpenAir System = new OpenAir();
            MemberRecord user = new MemberRecord();

            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            user.Login("ABC", "a123");

            Item Item1 = new Item();
            Item Item2 = new Item();

            System.SetType("Cloth");
            Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(300);
            Item1.SetIntro("舒適、好搭潮T");
            Item1.SetSellNum(0);
            Item1.SetType(0);

            Item2.SetName("Pants1");
            Item2.SetNum(100);
            Item2.SetPrice(350);
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            Assert.That(System.AddItem(Item1), Is.EqualTo(0));
            Assert.That(System.AddItem(Item2), Is.EqualTo(1));

            System.Close();
        }
        
        [Test]
        public void 增加不同類型商品()
        {
            OpenAir System = new OpenAir();
            MemberRecord user = new MemberRecord();

            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            user.Login("ABC", "a123");
            Item Item1 = new Item();
            Item Item2 = new Item();

            System.SetType("Cloth");
            Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(300);
            Item1.SetIntro("舒適、好搭潮T");
            Item1.SetSellNum(0);
            Item1.SetType(0);

            Item2.SetName("Pants1");
            Item2.SetNum(100);
            Item2.SetPrice(350);
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            System.AddItem(Item1);
            System.AddItem(Item2);


            Item Item3 = new Item();
            Item Item4 = new Item();

            System.SetType("Games");
            Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));


            Item3.SetName("diablo III");
            Item3.SetNum(99);
            Item3.SetPrice(1099);
            Item3.SetIntro("專搶你男朋友");
            Item3.SetSellNum(3);
            Item3.SetType(1);

            Item4.SetName("THE MESSAGE");
            Item4.SetNum(333);
            Item4.SetPrice(500);
            Item4.SetIntro("風聲 桌遊");
            Item4.SetSellNum(33);
            Item4.SetType(1);

            Assert.That(System.AddItem(Item3), Is.EqualTo(2));
            Assert.That(System.AddItem(Item4), Is.EqualTo(3));

            System.Close();

        }

        [Test]
        public void 增加兩個以上不同類型商品()
        {
            OpenAir System = new OpenAir();
            MemberRecord user = new MemberRecord();

            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            user.Login("ABC", "a123");
            Item Item1 = new Item();
            Item Item2 = new Item();

            System.SetType("Cloth");
            Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(300);
            Item1.SetIntro("舒適、好搭潮T");
            Item1.SetSellNum(0);
            Item1.SetType(0);

            Item2.SetName("Pants1");
            Item2.SetNum(100);
            Item2.SetPrice(350);
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            System.AddItem(Item1);
            System.AddItem(Item2);

            Item Item3 = new Item();
            Item Item4 = new Item();

            System.SetType("Games");
            Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));


            Item3.SetName("diablo III");
            Item3.SetNum(99);
            Item3.SetPrice(1099);
            Item3.SetIntro("專搶你男朋友");
            Item3.SetSellNum(3);
            Item3.SetType(1);

            Item4.SetName("THE MESSAGE");
            Item4.SetNum(333);
            Item4.SetPrice(500);
            Item4.SetIntro("風聲 桌遊");
            Item4.SetSellNum(33);
            Item4.SetType(1);

            System.AddItem(Item3);
            System.AddItem(Item4);

            Item Item5 = new Item();
            Item Item6 = new Item();

            System.SetType("Games");
            Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));


            Item5.SetName("diablo III");
            Item5.SetNum(99);
            Item5.SetPrice(1099);
            Item5.SetIntro("專搶你男朋友");
            Item5.SetSellNum(3);
            Item5.SetType(1);

            Item6.SetName("THE MESSAGE");
            Item6.SetNum(333);
            Item6.SetPrice(500);
            Item6.SetIntro("風聲 桌遊");
            Item6.SetSellNum(33);
            Item6.SetType(1);

            Assert.That(System.AddItem(Item5), Is.EqualTo(4));
            Assert.That(System.AddItem(Item6), Is.EqualTo(5));

            System.Close();

        }

        [Test]
        public void 搜尋商品()
        {

            OpenAir System = new OpenAir();
            MemberRecord user = new MemberRecord();

            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            user.Login("ABC", "a123");
            Item Item1 = new Item();
            Item Item2 = new Item();
            
            System.SetType("Cloth");
            Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(300);
            Item1.SetIntro("舒適、好搭潮T");
            Item1.SetSellNum(0);
            Item1.SetType(0);

            Item2.SetName("Pants1");
            Item2.SetNum(100);
            Item2.SetPrice(350);
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            System.AddItem(Item1);
            System.AddItem(Item2);

            Item Item3 = new Item();
            Item Item4 = new Item();

            System.SetType("Games");
            Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));


            Item3.SetName("diablo III");
            Item3.SetNum(99);
            Item3.SetPrice(1099);
            Item3.SetIntro("專搶你男朋友");
            Item3.SetSellNum(3);
            Item3.SetType(1);

            Item4.SetName("THE MESSAGE");
            Item4.SetNum(333);
            Item4.SetPrice(500);
            Item4.SetIntro("風聲 桌遊");
            Item4.SetSellNum(33);
            Item4.SetType(1);

            System.AddItem(Item3);
            System.AddItem(Item4);

            Item Item5 = new Item();
            Item Item6 = new Item();

            System.SetType("Games");
            Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));


            Item5.SetName("diablo III");
            Item5.SetNum(99);
            Item5.SetPrice(1099);
            Item5.SetIntro("專搶你男朋友");
            Item5.SetSellNum(3);
            Item5.SetType(1);

            Item6.SetName("THE MESSAGE");
            Item6.SetNum(333);
            Item6.SetPrice(500);
            Item6.SetIntro("風聲 桌遊");
            Item6.SetSellNum(33);
            Item6.SetType(1);

            System.AddItem(Item5);
            System.AddItem(Item6);

            Assert.That(System.GetItemDataCount(), Is.EqualTo(6));

            int[] search1 = { 1 };
            Assert.That(System.Search("Pants1"), Is.EqualTo(search1));

            int[] search2 = { 0 };
            Assert.That(System.Search("Cloth1"), Is.EqualTo(search2));

            System.Close();
        }

        [Test]
        public void 搜尋兩個以上商品()
        {

            OpenAir System = new OpenAir();
            MemberRecord user = new MemberRecord();

            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            user.Login("ABC", "a123");
            Item Item1 = new Item();
            Item Item2 = new Item();
            

           System.SetType("Cloth");
           Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(300);
            Item1.SetIntro("舒適、好搭潮T");
            Item1.SetSellNum(0);
            Item1.SetType(0);

            Item2.SetName("Pants1");
            Item2.SetNum(100);
            Item2.SetPrice(350);
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            System.AddItem(Item1);
            System.AddItem(Item2);

            Item Item3 = new Item();
            Item Item4 = new Item();

            System.SetType("Games");
            Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));


            Item3.SetName("diablo III");
            Item3.SetNum(99);
            Item3.SetPrice(1099);
            Item3.SetIntro("專搶你男朋友");
            Item3.SetSellNum(3);
            Item3.SetType(1);

            Item4.SetName("THE MESSAGE");
            Item4.SetNum(333);
            Item4.SetPrice(500);
            Item4.SetIntro("風聲 桌遊");
            Item4.SetSellNum(33);
            Item4.SetType(1);

            System.AddItem(Item3);
            System.AddItem(Item4);

            Item Item5 = new Item();
            Item Item6 = new Item();

            System.SetType("Games");
            Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));


            Item5.SetName("diablo III");
            Item5.SetNum(99);
            Item5.SetPrice(1099);
            Item5.SetIntro("專搶你男朋友");
            Item5.SetSellNum(3);
            Item5.SetType(1);

            Item6.SetName("THE MESSAGE");
            Item6.SetNum(333);
            Item6.SetPrice(500);
            Item6.SetIntro("風聲 桌遊");
            Item6.SetSellNum(33);
            Item6.SetType(1);

            System.AddItem(Item5);
            System.AddItem(Item6);

            Item Item7 = new Item();

            Item7.SetName("THE MESSAGE");
            Item7.SetNum(99);
            Item7.SetPrice(1099);
            Item7.SetIntro("I need money!");
            Item7.SetSellNum(3);
            Item7.SetType(1);

            System.AddItem(Item7);

            Assert.That(System.GetItemDataCount(), Is.EqualTo(7));

            int[] search1 = { 2, 4 };
            Assert.That(System.Search("diablo III"), Is.EqualTo(search1));

            int[] search2 = { 3, 5, 6 };
            Assert.That(System.Search("THE MESSAGE"), Is.EqualTo(search2));

            System.Close();
        }

        [Test]
        public void 更改商品資訊()
        {
            OpenAir System = new OpenAir();
            MemberRecord user = new MemberRecord();

            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            user.Login("ABC", "a123");

            Item Item1 = new Item();
            Item Item2 = new Item();
           

            System.SetType("Cloth");
            Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(300);
            Item1.SetIntro("舒適、好搭潮T");
            Item1.SetSellNum(0);
            Item1.SetType(0);

            Item2.SetName("Pants1");
            Item2.SetNum(100);
            Item2.SetPrice(350);
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            Assert.That(System.AddItem(Item1), Is.EqualTo(0));
            Assert.That(System.AddItem(Item2), Is.EqualTo(1));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(250);
            Item1.SetIntro("便宜，舒適、好搭潮T");
            Item1.SetSellNum(55);
            Item1.SetType(0);

            Assert.That(System.ChangeItem(Item1, 0), Is.EqualTo(true));
            Assert.That(Item1.GetPrice(), Is.EqualTo(250));
            Assert.That(System.GetItemDataPrice(0), Is.EqualTo(250));

            Item2.SetName("Pants1");
            Item2.SetNum(100);
            Item2.SetPrice(299);
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            Assert.That(System.ChangeItem(Item2, 1), Is.EqualTo(true));
            Assert.That(Item2.GetPrice(), Is.EqualTo(299));
            Assert.That(System.GetItemDataPrice(1), Is.EqualTo(299));
            

            System.Close();

        }

        [Test]
        public void 商品下架()
        {
            OpenAir System = new OpenAir();
            MemberRecord user = new MemberRecord();

            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            user.Login("ABC", "a123");

            Item Item1 = new Item();
            Item Item2 = new Item();


            System.SetType("Cloth");
            Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(300);
            Item1.SetIntro("舒適、好搭潮T");
            Item1.SetSellNum(0);
            Item1.SetType(0);

            Item2.SetName("Pants1");
            Item2.SetNum(100);
            Item2.SetPrice(350);
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            Assert.That(System.AddItem(Item1), Is.EqualTo(0));
            Assert.That(System.AddItem(Item2), Is.EqualTo(1));

            Assert.That(System.DelItem(0), Is.EqualTo(true));
            Assert.That(System.GetItemDeleteOrNot(0), Is.EqualTo(true));


            Assert.That(System.GetItemDeleteOrNot(1), Is.EqualTo(false));   //檢查沒被刪之item的delete狀態

            Assert.That(System.DelItem(3), Is.EqualTo(false));              //刪除非編號中item，將失敗


            System.Close();
        }
        
        [Test]
        public void 增加同樣的商品數量()
        {
            OpenAir System = new OpenAir();
            MemberRecord user = new MemberRecord();

            user.Register("ABC", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taiwan");
            user.Login("ABC", "a123");

            Item Item1 = new Item();
            Item Item2 = new Item();


            System.SetType("Cloth");
            Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(300);
            Item1.SetIntro("舒適、好搭潮T");
            Item1.SetSellNum(0);
            Item1.SetType(0);

            Item2.SetName("Pants1");
            Item2.SetNum(100);
            Item2.SetPrice(350);
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            Assert.That(System.AddItem(Item1), Is.EqualTo(0));
            Assert.That(System.AddItem(Item2), Is.EqualTo(1));

            Assert.That(System.getItemNum(0), Is.EqualTo(100));
            Item1.SetNum(200);

            Assert.That(System.ChangeItem(Item1, 0), Is.EqualTo(true));
            Assert.That(Item1.GetNum(), Is.EqualTo(200));
            Assert.That(System.getItemNum(0), Is.EqualTo(200));

            Assert.That(System.getItemNum(1), Is.EqualTo(100));
            Item2.SetNum(999);

            Assert.That(System.ChangeItem(Item2, 1), Is.EqualTo(true));
            Assert.That(Item2.GetNum(), Is.EqualTo(999));
            Assert.That(System.getItemNum(1), Is.EqualTo(999));


            System.Close();

        }

        [Test]
        public void 加入訂單()
        {
            OpenAir System = new OpenAir();
            MemberRecord user1 = new MemberRecord();
            MemberRecord user2 = new MemberRecord();

            Assert.That(user1.Register("zxc", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taipei"), Is.EqualTo(true));
            Assert.That(user2.Register("qwert", "321a", "Brown", "987654321", "Brown@gmail.com", "Tainan"), Is.EqualTo(true));
            user1 = user1.Login("zxc", "a123");
            user2 = user2.Login("qwert", "321a");

            Item Item1 = new Item();
            Item Item2 = new Item();

            if (System.IsLogin(user1))
            {
                System.SetType("Cloth");
                Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

                Item1.SetName("Cloth1");
                Item1.SetNum(100);
                Item1.SetPrice(300);
                Item1.SetSeller("zxc");
                Item1.SetIntro("舒適、好搭潮T");
                Item1.SetSellNum(0);
                Item1.SetType(0);

                Item2.SetName("Pants1");
                Item2.SetNum(0);
                Item2.SetPrice(350);
                Item2.SetSeller("zxc");
                Item2.SetIntro("顯瘦舒適");
                Item2.SetSellNum(0);
                Item2.SetType(0);

                System.AddItem(Item1);
                System.AddItem(Item2);
            }
            if (System.IsLogin(user2))
            {
                Assert.That(user2.Buy(Item1), Is.EqualTo("Cloth1"));
                Assert.That(user2.Buy(Item2), Is.EqualTo("目前無庫存!!"));

                Orders list1 = new Orders();

                list1.SetOrderID("201406060001");
                list1.SetOrderItemName(Item1.GetItemName());
                list1.SetOrderBuyer(user2.GetID());
                list1.SetOrderSeller(Item1.GetSeller());
                list1.SetOrderAddress(user2.GetAddress());
                list1.SetOrderQuantity(50);
                Item1.SetSellNum(list1.CheckQuantity(Item1.GetNum(), list1.GetOrderQuantity()));
                Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
                list1.SetOrderPrice(Item1.GetSellNum() * Item1.GetPrice());
                list1.SetPayChoose(0);

                Assert.That(list1.GetOrderID(), Is.EqualTo("201406060001"));
                Assert.That(list1.GetOrderName(), Is.EqualTo("Cloth1"));
                Assert.That(list1.GetOrderQuantity(), Is.EqualTo(50));
                Assert.That(list1.GetOrderPrice(), Is.EqualTo(300 * 50));
                Assert.That(list1.GetOrderBuyer(), Is.EqualTo(user2.GetID()));
                Assert.That(list1.GetOrderSeller(), Is.EqualTo(Item1.GetSeller()));
                Assert.That(list1.AddOrder(list1), Is.EqualTo(1));
                Assert.That(list1.GetOrderAddress(), Is.EqualTo(user2.GetAddress()));
                Assert.That(list1.GetPayChoose(), Is.EqualTo("超商付款"));
                Assert.That(list1.CheckOrderSuccess(), Is.EqualTo("立即為您出貨"));
                Assert.That(Item1.GetSellNum(), Is.EqualTo(50));
                Assert.That(Item1.GetNum(), Is.EqualTo(50));


                Orders list2 = new Orders();

                list2.SetOrderID("201406060002");
                list2.SetOrderItemName(Item1.GetItemName());
                list2.SetOrderBuyer(user2.GetID());
                list2.SetOrderSeller(Item1.GetSeller());
                list2.SetOrderQuantity(10);
                Item1.SetSellNum(list2.CheckQuantity(Item1.GetNum(), list2.GetOrderQuantity()));
                Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
                list2.SetOrderPrice(Item1.GetSellNum() * Item1.GetPrice());
                list2.SetPayChoose(1);

                Assert.That(list2.GetOrderID(), Is.EqualTo("201406060002"));
                Assert.That(list2.GetOrderName(), Is.EqualTo("Cloth1"));
                Assert.That(list2.GetOrderPrice(), Is.EqualTo(300 * 10));
                Assert.That(list2.GetOrderBuyer(), Is.EqualTo(user2.GetID()));
                Assert.That(list2.GetOrderSeller(), Is.EqualTo(Item1.GetSeller()));
                Assert.That(list2.AddOrder(list2), Is.EqualTo(2));
                Assert.That(list2.GetPayChoose(), Is.EqualTo("ATM轉帳"));
                Assert.That(list2.CheckOrderSuccess(), Is.EqualTo("立即為您出貨"));
                Assert.That(Item1.GetSellNum(), Is.EqualTo(10));
                Assert.That(Item1.GetNum(), Is.EqualTo(40));


                Orders list3 = new Orders();

                list3.SetOrderID("201406080001");
                list3.SetOrderItemName(Item1.GetItemName());
                list3.SetOrderBuyer(user2.GetID());
                list3.SetOrderSeller(Item1.GetSeller());
                list3.SetOrderQuantity(50);
                Item1.SetSellNum(list3.CheckQuantity(Item1.GetNum(), list3.GetOrderQuantity()));
                Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
                list3.SetOrderPrice(Item1.GetSellNum() * Item1.GetPrice());
                list3.SetPayChoose(1);

                Assert.That(list3.GetOrderID(), Is.EqualTo("201406080001"));
                Assert.That(list3.GetOrderName(), Is.EqualTo("Cloth1"));
                Assert.That(list3.GetOrderPrice(), Is.EqualTo(300 * Item1.GetSellNum()));
                Assert.That(list3.GetOrderBuyer(), Is.EqualTo("qwert"));
                Assert.That(list3.GetOrderSeller(), Is.EqualTo("zxc"));
                Assert.That(list3.AddOrder(list3), Is.EqualTo(3));
                Assert.That(list3.GetPayChoose(), Is.EqualTo("ATM轉帳"));
                Assert.That(list3.CheckOrderSuccess(), Is.EqualTo("立即為您出貨"));
                Assert.That(Item1.GetSellNum(), Is.EqualTo(40));
                Assert.That(Item1.GetNum(), Is.EqualTo(0));
            }

            System.Close();
            
        }
        
        [Test]
        public void 取消訂單()
        {

            OpenAir System = new OpenAir();
            
            MemberRecord user1 = new MemberRecord();
            MemberRecord user2 = new MemberRecord();
       
            Assert.That(user1.Register("zxce", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taipei"), Is.EqualTo(true));
            Assert.That(user2.Register("qwert", "321a", "Brown", "987654321", "Brown@gmail.com", "Tainan"), Is.EqualTo(true));
            user1 = user1.Login("zxce", "a123");
            user2 = user2.Login("qwert", "321a");

            Item Item1 = new Item();
            Item Item2 = new Item();
           
            System.SetType("Cloth");
            Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

            Item1.SetName("Cloth1");
            Item1.SetNum(100);
            Item1.SetPrice(300);
            Item1.SetSeller("zxce");
            Item1.SetIntro("舒適、好搭潮T");
            Item1.SetSellNum(0);
            Item1.SetType(0);

            Item2.SetName("Pants1");
            Item2.SetNum(0);
            Item2.SetPrice(350);
            Item2.SetSeller("zxce");
            Item2.SetIntro("顯瘦舒適");
            Item2.SetSellNum(0);
            Item2.SetType(0);

            System.AddItem(Item1);
            System.AddItem(Item2);

            Assert.That(user2.Buy(Item1), Is.EqualTo("Cloth1"));
            Assert.That(user2.Buy(Item2), Is.EqualTo("目前無庫存!!"));


            Orders list1 = new Orders();

            list1.SetOrderID("201406060001");
            list1.SetOrderItemName(Item1.GetItemName());
            list1.SetOrderBuyer(user2.GetID());
            list1.SetOrderSeller(Item1.GetSeller());
            list1.SetOrderQuantity(50);
            Item1.SetSellNum(list1.CheckQuantity(Item1.GetNum(), list1.GetOrderQuantity()));
            Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
            list1.SetOrderPrice(Item1.GetSellNum() * Item1.GetPrice());
            list1.SetPayChoose(0);


            Assert.That(list1.GetPayChoose(), Is.EqualTo("超商付款"));
            Assert.That(list1.CheckOrderSuccess(), Is.EqualTo("立即為您出貨"));
                    

            Orders list2 = new Orders();

            list2.SetOrderID("201406060002");
            list2.SetOrderItemName(Item1.GetItemName());
            list2.SetOrderBuyer(user2.GetID());
            list2.SetOrderSeller(Item1.GetSeller());
            list2.SetOrderQuantity(10);
            Item1.SetSellNum(list2.CheckQuantity(Item1.GetNum(), list2.GetOrderQuantity()));
            Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
            list2.SetOrderPrice(Item1.GetSellNum() * Item1.GetPrice());
            list2.SetPayChoose(1);

            Assert.That(list2.GetPayChoose(), Is.EqualTo("ATM轉帳"));
            Assert.That(list2.CheckOrderSuccess(), Is.EqualTo("立即為您出貨"));
            Assert.That(list2.CancelOrder(list2.GetOrderID()), Is.EqualTo("訂單取消成功"));

            System.Close();
        }

        [Test]
        public void 搜尋訂單()
        {
            OpenAir System = new OpenAir();

            MemberRecord user1 = new MemberRecord();
            MemberRecord user2 = new MemberRecord();

            Assert.That(user1.Register("zxce", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taipei"), Is.EqualTo(true));
            Assert.That(user2.Register("qwert", "321a", "Brown", "987654321", "Brown@gmail.com", "Tainan"), Is.EqualTo(true));
            user1 = user1.Login("zxce", "a123");
            user2 = user2.Login("qwert", "321a");

            Item Item1 = new Item();
            Item Item2 = new Item();

            Orders list1 = new Orders();

            list1.SetOrderID("201406060001");
            list1.SetOrderItemName(Item1.GetItemName());
            list1.SetOrderBuyer(user2.GetID());
            list1.SetOrderSeller(Item1.GetSeller());
            list1.SetOrderQuantity(50);
            Item1.SetSellNum(list1.CheckQuantity(Item1.GetNum(), list1.GetOrderQuantity()));
            Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
            list1.SetOrderPrice(Item1.GetSellNum() * Item1.GetPrice());
            list1.SetPayChoose(0);

            Assert.That(list1.GetPayChoose(), Is.EqualTo("超商付款"));
            Assert.That(list1.CheckOrderSuccess(), Is.EqualTo("立即為您出貨"));


            Orders list2 = new Orders();

            list2.SetOrderID("201406060002");
            list2.SetOrderItemName(Item1.GetItemName());
            list2.SetOrderBuyer(user2.GetID());
            list2.SetOrderSeller(Item1.GetSeller());
            list2.SetOrderQuantity(0);
            Item1.SetSellNum(list2.CheckQuantity(Item1.GetNum(), list2.GetOrderQuantity()));
            Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
            list2.SetOrderPrice(Item1.GetSellNum() * Item1.GetPrice());
            list2.SetPayChoose(1);

            Assert.That(list2.GetPayChoose(), Is.EqualTo("ATM轉帳"));
            Assert.That(list2.CheckOrderSuccess(), Is.EqualTo("Failed"));

            Assert.That(list1.SearchOrder(list1.GetOrderID()), Is.EqualTo("搜尋到此編號訂單"));
            Assert.That(list2.SearchOrder(list2.GetOrderID()), Is.EqualTo("查無此訂單"));


            System.Close();
        }

        [Test]
        public void 交付和取得物品()
        {
            OpenAir System = new OpenAir();
            
            MemberRecord user1 = new MemberRecord();
            MemberRecord user2 = new MemberRecord();
        
            Assert.That(user1.Register("zxc", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taipei"), Is.EqualTo(true));
            Assert.That(user2.Register("qwert", "321a", "Brown", "987654321", "Brown@gmail.com", "Tainan"), Is.EqualTo(true));
            user1 = user1.Login("zxc", "a123");
            user2 = user2.Login("qwert", "321a");

            Item Item1 = new Item();
            Item Item2 = new Item();
            

            if (System.IsLogin(user1))
            {
                System.SetType("Cloth");
                Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

                Item1.SetName("Cloth1");
                Item1.SetNum(100);
                Item1.SetPrice(300);
                Item1.SetSeller("zxc");
                Item1.SetIntro("舒適、好搭潮T");
                Item1.SetSellNum(0);
                Item1.SetType(0);

                Item2.SetName("Pants1");
                Item2.SetNum(0);
                Item2.SetPrice(350);
                Item2.SetSeller("zxc");
                Item2.SetIntro("顯瘦舒適");
                Item2.SetSellNum(0);
                Item2.SetType(0);

                System.AddItem(Item1);
                System.AddItem(Item2);
            }

            if (System.IsLogin(user2))
            {
                Assert.That(user2.Buy(Item1), Is.EqualTo("Cloth1"));
                Assert.That(user2.Buy(Item2), Is.EqualTo("目前無庫存!!"));

                Orders list1 = new Orders();

                list1.SetOrderID("201406060001");
                list1.SetOrderItemName(Item1.GetItemName());
                list1.SetOrderBuyer(user2.GetID());
                list1.SetOrderSeller(Item1.GetSeller());
                list1.SetOrderQuantity(50);
                Item1.SetSellNum(list1.CheckQuantity(Item1.GetNum(), list1.GetOrderQuantity()));
                Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
                list1.SetOrderPrice(Item1.GetSellNum() * Item1.GetPrice());
                list1.SetPayChoose(0);

                Assert.That(list1.GetPayChoose(), Is.EqualTo("超商付款"));
                Assert.That(list1.CheckOrderSuccess(), Is.EqualTo("立即為您出貨"));
                    
                Orders list2 = new Orders();

                list2.AddOrder(list2);
                list2.SetOrderID("201406060002");
                list2.SetOrderItemName(Item1.GetItemName());
                list2.SetOrderPrice(Item1.GetPrice());
                list2.SetOrderQuantity(10);
                list2.SetOrderBuyer(user2.GetID());
                list2.SetOrderSeller(Item1.GetSeller());
                Item1.SetSellNum(list2.CheckQuantity(Item1.GetNum(), list2.GetOrderQuantity()));
                Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
                list2.SetPayChoose(1);

                Assert.That(list2.GetPayChoose(), Is.EqualTo("ATM轉帳"));
                Assert.That(list1.CheckOrderSuccess(), Is.EqualTo("立即為您出貨"));
                Assert.That(list2.CancelOrder(list2.GetOrderID()), Is.EqualTo("訂單取消成功"));
            }

            user1.DeliverItem(Item1,user2);
            user2.GainItem(Item1,user1);

            List<Item> user1SellList = user1.GetSellItemList();
            List<Item> user2BuyList = user2.GetBuyItemList();

            Assert.That(user1SellList[0], Is.EqualTo(Item1));
            Assert.That(user2BuyList[0], Is.EqualTo(Item1));

            System.Close();
        }

        [Test]
        public void 退貨()
        {
            OpenAir System = new OpenAir();
            
            MemberRecord user1 = new MemberRecord();
            MemberRecord user2 = new MemberRecord();

            Assert.That(user1.Register("zxc", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taipei"), Is.EqualTo(true));
            Assert.That(user2.Register("qwert", "321a", "Brown", "987654321", "Brown@gmail.com", "Tainan"), Is.EqualTo(true));
            user1 = user1.Login("zxc", "a123");
            user2 = user2.Login("qwert", "321a");

            Item Item1 = new Item();
            Item Item2 = new Item();

            if (System.IsLogin(user1))
            {
                System.SetType("Cloth");
                Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

                Item1.SetName("Cloth1");
                Item1.SetNum(100);
                Item1.SetPrice(300);
                Item1.SetSeller("zxc");
                Item1.SetIntro("舒適、好搭潮T");
                Item1.SetSellNum(0);
                Item1.SetType(0);

                System.AddItem(Item1);
            }

            if (System.IsLogin(user2))
            {
                Assert.That(user2.Buy(Item1), Is.EqualTo("Cloth1"));

                Orders list1 = new Orders();

                list1.SetOrderID("201406060001");
                list1.SetOrderItemName(Item1.GetItemName());
                list1.SetOrderBuyer(user2.GetID());
                list1.SetOrderSeller(Item1.GetSeller());
                list1.SetOrderQuantity(50);
                Item1.SetSellNum(list1.CheckQuantity(Item1.GetNum(), list1.GetOrderQuantity()));
                Item1.SetNum(Item1.GetNum() - Item1.GetSellNum());
                list1.SetOrderPrice(Item1.GetSellNum() * Item1.GetPrice());
                list1.SetPayChoose(0);

                Assert.That(list1.GetPayChoose(), Is.EqualTo("超商付款"));
                Assert.That(list1.CheckOrderSuccess(), Is.EqualTo("立即為您出貨"));
                    
            }

            List<Item> user1SellList = user1.GetSellItemList();
            List<Item> user2BuyList = user2.GetBuyItemList();

            user1.DeliverItem(Item1, user2);
            user2.GainItem(Item1, user1);

            Assert.That(user1SellList.Count, Is.EqualTo(1));
            Assert.That(user2BuyList.Count, Is.EqualTo(1));

            user2.ReturnItem(Item1, user1);

            Assert.That(user1SellList.Count, Is.EqualTo(1));
            Assert.That(user2BuyList.Count, Is.EqualTo(0));

            System.Close();
        }

        [Test]
        public void 列出分類的商品()
        {
            OpenAir System = new OpenAir();
            MemberRecord user = new MemberRecord();

            user.Register("zxc", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taipei");
            user = user.Login("zxc", "a123");

            Item Item1 = new Item();
            Item Item2 = new Item();
            Item Item3 = new Item();
            Item Item4 = new Item();

            if( System.IsLogin(user))
            {
                System.SetType("Cloth");
                Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

                Item1.SetName("Cloth1");
                Item1.SetNum(100);
                Item1.SetPrice(300);
                Item1.SetSeller("zxc");
                Item1.SetIntro("舒適、好搭潮T");
                Item1.SetSellNum(0);
                Item1.SetType(0);

                Item2.SetName("Pants1");
                Item2.SetNum(0);
                Item2.SetPrice(350);
                Item2.SetSeller("zxc");
                Item2.SetIntro("顯瘦舒適");
                Item2.SetSellNum(0);
                Item2.SetType(0);

                System.AddItem(Item1);
                System.AddItem(Item2);

                System.SetType("Games");
                Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));

                Item3.SetName("diablo III");
                Item3.SetNum(99);
                Item3.SetPrice(1099);
                Item3.SetSeller("zxc");
                Item3.SetIntro("專搶你男朋友");
                Item3.SetSellNum(3);
                Item3.SetType(1);

                Item4.SetName("THE MESSAGE");
                Item4.SetNum(333);
                Item4.SetPrice(500);
                Item4.SetSeller("zxc");
                Item4.SetIntro("風聲 桌遊");
                Item4.SetSellNum(33);
                Item4.SetType(1);

                System.AddItem(Item3);
                System.AddItem(Item4);
            }

            List<Item> clothesList = new List<Item>();
            clothesList = System.ShowClassificationOfGoods("Cloth");

            Assert.That(clothesList[0].GetItemName(), Is.EqualTo("Cloth1"));
            Assert.That(clothesList[1].GetItemName(), Is.EqualTo("Pants1"));

            List<Item> gamesList = new List<Item>();
            gamesList = System.ShowClassificationOfGoods("Games");

            Assert.That(gamesList[0].GetItemName(), Is.EqualTo("diablo III"));
            Assert.That(gamesList[1].GetItemName(), Is.EqualTo("THE MESSAGE"));

            System.Close();
        }



        [Test]
        public void 加入商品至購物車()
        {
            OpenAir System = new OpenAir();

            MemberRecord user1 = new MemberRecord();
            MemberRecord user2 = new MemberRecord();

            Assert.That(user1.Register("zxc", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taipei"), Is.EqualTo(true));
            Assert.That(user2.Register("qwert", "321a", "Brown", "987654321", "Brown@gmail.com", "Tainan"), Is.EqualTo(true));
            user1 = user1.Login("zxc", "a123");
            user2 = user2.Login("qwert", "321a");

            Item Item1 = new Item();
            Item Item2 = new Item();
            Item Item3 = new Item();
            Item Item4 = new Item();
            Item Item5 = new Item();
            Item Item6 = new Item();

            if (System.IsLogin(user1))
            {
                System.SetType("Cloth");
                Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

                Item1.SetName("Cloth1");
                Item1.SetNum(100);
                Item1.SetPrice(300);
                Item1.SetIntro("舒適、好搭潮T");
                Item1.SetSellNum(0);
                Item1.SetType(0);

                Item2.SetName("Pants1");
                Item2.SetNum(100);
                Item2.SetPrice(350);
                Item2.SetIntro("顯瘦舒適");
                Item2.SetSellNum(0);
                Item2.SetType(0);

                System.AddItem(Item1);
                System.AddItem(Item2);

                System.SetType("Games");
                Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));


                Item3.SetName("diablo III");
                Item3.SetNum(99);
                Item3.SetPrice(1099);
                Item3.SetIntro("專搶你男朋友");
                Item3.SetSellNum(3);
                Item3.SetType(1);

                Item4.SetName("THE MESSAGE");
                Item4.SetNum(333);
                Item4.SetPrice(500);
                Item4.SetIntro("風聲 桌遊");
                Item4.SetSellNum(33);
                Item4.SetType(1);

                System.AddItem(Item3);
                System.AddItem(Item4);

                System.SetType("Games");
                Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));

                Item5.SetName("diablo III");
                Item5.SetNum(99);
                Item5.SetPrice(1099);
                Item5.SetIntro("專搶你男朋友");
                Item5.SetSellNum(3);
                Item5.SetType(1);

                Item6.SetName("THE MESSAGE");
                Item6.SetNum(333);
                Item6.SetPrice(500);
                Item6.SetIntro("風聲 桌遊");
                Item6.SetSellNum(33);
                Item6.SetType(1);
            }

            ShoppingCart buyerCart = new ShoppingCart();
            if (System.IsLogin(user2))
            {
                buyerCart.Put(Item2);
                buyerCart.Put(Item3);
                buyerCart.Put(Item6);
            }

            List<Item> shoppingCartList = buyerCart.Get();
            Assert.That(shoppingCartList[0], Is.EqualTo(Item2));
            Assert.That(shoppingCartList[1], Is.EqualTo(Item3));
            Assert.That(shoppingCartList[2], Is.EqualTo(Item6));

            System.Close();
        }

        [Test]
        public void 刪除購物車商品()
        {
            OpenAir System = new OpenAir();

            MemberRecord user1 = new MemberRecord();
            MemberRecord user2 = new MemberRecord();

            Assert.That(user1.Register("zxc", "a123", "Joe", "0123456789", "Joe@gmail.com", "Taipei"), Is.EqualTo(true));
            Assert.That(user2.Register("qwert", "321a", "Brown", "987654321", "Brown@gmail.com", "Tainan"), Is.EqualTo(true));
            user1 = user1.Login("zxc", "a123");
            user2 = user2.Login("qwert", "321a");

            Item Item1 = new Item();
            Item Item2 = new Item();
            Item Item3 = new Item();
            Item Item4 = new Item();
            Item Item5 = new Item();
            Item Item6 = new Item();

            if (System.IsLogin(user1))
            {
                System.SetType("Cloth");
                Assert.That(System.GetTypeNum("Cloth"), Is.EqualTo(0));

                Item1.SetName("Cloth1");
                Item1.SetNum(100);
                Item1.SetPrice(300);
                Item1.SetIntro("舒適、好搭潮T");
                Item1.SetSellNum(0);
                Item1.SetType(0);

                Item2.SetName("Pants1");
                Item2.SetNum(100);
                Item2.SetPrice(350);
                Item2.SetIntro("顯瘦舒適");
                Item2.SetSellNum(0);
                Item2.SetType(0);

                System.AddItem(Item1);
                System.AddItem(Item2);

                System.SetType("Games");
                Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));


                Item3.SetName("diablo III");
                Item3.SetNum(99);
                Item3.SetPrice(1099);
                Item3.SetIntro("專搶你男朋友");
                Item3.SetSellNum(3);
                Item3.SetType(1);

                Item4.SetName("THE MESSAGE");
                Item4.SetNum(333);
                Item4.SetPrice(500);
                Item4.SetIntro("風聲 桌遊");
                Item4.SetSellNum(33);
                Item4.SetType(1);

                System.AddItem(Item3);
                System.AddItem(Item4);

                System.SetType("Games");
                Assert.That(System.GetTypeNum("Games"), Is.EqualTo(1));

                Item5.SetName("diablo III");
                Item5.SetNum(99);
                Item5.SetPrice(1099);
                Item5.SetIntro("專搶你男朋友");
                Item5.SetSellNum(3);
                Item5.SetType(1);

                Item6.SetName("THE MESSAGE");
                Item6.SetNum(333);
                Item6.SetPrice(500);
                Item6.SetIntro("風聲 桌遊");
                Item6.SetSellNum(33);
                Item6.SetType(1);
            }

            ShoppingCart buyerCart = new ShoppingCart();
            if (System.IsLogin(user2))
            {
                buyerCart.Put(Item2);
                buyerCart.Put(Item3);
                buyerCart.Put(Item6);

                List<Item> shoppingCartList = buyerCart.Get();
                Assert.That(shoppingCartList[0], Is.EqualTo(Item2));
                Assert.That(shoppingCartList[1], Is.EqualTo(Item3));
                Assert.That(shoppingCartList[2], Is.EqualTo(Item6));

                buyerCart.Delete(Item3);
                Assert.That(shoppingCartList[0], Is.EqualTo(Item2));
                Assert.That(shoppingCartList[1], Is.EqualTo(Item6));

            }

            System.Close();
        }

    }
}
