﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenAirLibrary
{
    class OpenAir
    {
        static private List<MemberRecord> MemberData = new List<MemberRecord>();
        static private List<Item> ItemData = new List<Item>();
        static private List<string> TypeData = new List<string>();
        static private List<string> IsUserlogin = new List<string>();
        static private int memberLoginTotalCount;

        internal void AddAccount(MemberRecord member)
        {
            MemberData.Add(member);
        }

        internal MemberRecord GetAccount(string id, string password)
        {
            int i = 0;
            while (i != MemberData.Count && CheckIDAndPassword(i, id, password))
            {
                i++;
            }
            if (MemberData.Count == i)
                return null;

            return MemberData[i];
        }

        internal bool CheckIDAndPassword(int i, string id, string password)
        {
            return !MemberData[i].GetID().Equals(id) || !MemberData[i].GetPassword().Equals(password);
        }

        internal int AddItem(Item i)
        {
            //i.setSeller();
            ItemData.Add(i);
            return (ItemData.Count - 1);
        }

        internal void SetType(string tname)
        {
            for (int i = 0; i < TypeData.Count; i++)
            {
                if (TypeData[i].Equals(tname))
                    return;
            }
            TypeData.Add(tname);
        }

        internal int GetTypeNum(string tname)
        {
            for (int i = 0; i < TypeData.Count; i++)
            {
                if (TypeData[i].Equals(tname))
                    return i;
            }
            return -1;
        }

        internal double GetItemDataPrice(int index)
        {
            return ItemData[index].GetPrice();
        }

        internal bool GetItemDeleteOrNot(int index)
        {
            return ItemData[index].GetDelete();
        }

        internal int getItemNum(int index)
        {

            return ItemData[index].GetNum();

        }

        internal int GetItemDataCount()
        {
            return ItemData.Count;
        }

        internal int[] Search(string name)
        {
            int sum = 0;
            int[] s;
            for (int i = 0; i < ItemData.Count; i++)
            {
                if (ItemData[i].GetItemName() == name)
                    sum++;
            }
            s = new int[sum];
            int nowI = 0;
            for (int i = 0; i < ItemData.Count; i++)
            {
                if (ItemData[i].GetItemName() == name)
                    s[nowI++] = i;
            }

            return s;
        }

        internal string GetAccountID(string id)
        {
            int i = 0;

            while (i != MemberData.Count && MemberData[i].GetID() != id)
                i++;

            if (i == MemberData.Count)
                return "";

            return MemberData[i].GetID();
        }

        internal void AddLoginUser(string id)
        {
            IsUserlogin.Add(id);
            memberLoginTotalCount = IsUserlogin.Count;
        }

        internal void RemoveLoginUser(string id)
        {
            for (int i = 0; i < memberLoginTotalCount; i++)
            {
                if (IsUserlogin[i].Equals(id))
                {
                    IsUserlogin.Remove(IsUserlogin[i]);
                    memberLoginTotalCount = IsUserlogin.Count;
                }
            }
        }

        internal int GetMemberLoginTotalCount()
        {
            return memberLoginTotalCount;
        }

        internal bool IsLogin(MemberRecord user)
        {
            for (int i = 0; i < memberLoginTotalCount; i++)
            {
                if (user.GetID().Equals(IsUserlogin[i]))
                    return true;
            }
            return false;
        }

        internal bool ChangeItem(Item i, int index)
        {

            //i.setSeller(loginuser);
            if (ItemData[index].GetSeller() == i.GetSeller())
            {
                ItemData[index].SetName(i.GetItemName());
                ItemData[index].SetNum(i.GetNum());
                ItemData[index].SetPrice(i.GetPrice());
                ItemData[index].SetIntro(i.GetIntro());
                ItemData[index].SetSellNum(i.GetSellNum());
                ItemData[index].SetType(i.GetType());
                return true;
            }
            return false;
        }

        internal bool DelItem(int index)
        {

            //i.setSeller(loginuser);
            if (index < ItemData.Count)
            {
                ItemData[index].SetDelete(true);
                return true;
            }
            return false;
        }

        internal void Close()
        {
            MemberData.Clear();
            ItemData.Clear();
            TypeData.Clear();
            IsUserlogin.Clear();
            memberLoginTotalCount = 0;
        }

        internal List<Item> ShowClassificationOfGoods(String typeData)
        {
            int i = 0;
            int typeNum = 0;
            List<Item> ClassificationOfGoods = new List<Item>();
            while (i < TypeData.Count && TypeData[i] != typeData)
            {
                i++;
            }

            typeNum = i;
            if (i < TypeData.Count && TypeData[i].Equals(typeData))
            {
                for (int j = 0; j < ItemData.Count; j++)
                    if (ItemData[j].GetType().Equals(typeNum))
                        ClassificationOfGoods.Add(ItemData[j]);
                return ClassificationOfGoods;
            }
            else
                return null;
        }

    }
}

