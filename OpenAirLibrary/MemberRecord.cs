﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenAirLibrary
{
    public class MemberRecord
    {
        private List<Item> sellItemList = new List<Item>();
        private List<Item> buyItemList = new List<Item>();

        private string id;
        private string password;
        private string name;
        private string phone;
        private string email;
        private string address;

        internal MemberRecord()
        {
           id = "";
           password = "";
           name = "";
           phone = "";
           email = "";
           address = "";
        }

        internal void SetMemberData(string id, string password, string name, string phone, string email, string address)
        {
            this.id = id;
            this.password = password;
            this.name = name;
            this.phone = phone;
            this.email = email;
            this.address = address;
        }

        internal bool Register(string id, string password, string name, string phone, string email, string address)
        {
            MemberRecord member = new MemberRecord();
            OpenAir system = new OpenAir();
            if (id.Trim() == "")
                return false;

            if (!CheckExist(id))
            {
                member.SetMemberData(id, password, name, phone, email, address);
                system.AddAccount(member);

                return true;
            }
            return false;
        }

        internal MemberRecord Login(string id, string password)
        {
            OpenAir system = new OpenAir();
            if (id.Equals(id) && password.Equals(password))
            {
                system.AddLoginUser(id);
                return system.GetAccount(id, password);
            }
            else
                return null;
        }

        internal void Logout()
        {
            OpenAir system = new OpenAir();
            system.RemoveLoginUser(id);
        }

        internal string Buy(Item item)
        {
            if (item.GetNum() > 0)
            {
                item.SetBuyer(id);
                return item.GetItemName();
            }

            return "目前無庫存!!";
        }

        internal string Modify(string passwd, string name, string phone, string email, string address)
        {
            SetMemberData(GetID(), passwd, name, phone, email, address);

            return "Successed";
        }

        internal string ModifyCheckPassword(string password)
        {
            if (GetPassword() == password)
                return "true";
            else
                return "密碼錯誤,請再重新輸入!!";
        }

        internal bool CheckExist(string id)
        {
            OpenAir system = new OpenAir();

            if (system.GetAccountID(id) == "")
                return false;
            else
                return true;
        }

        internal string GetID()
        {
            return this.id;
        }

        internal string GetPassword()
        {
            return this.password;
        }

        internal string GetName()
        {
            return this.name;
        }

        internal string GetPhone()
        {
            return this.phone;
        }

        internal string GetEmail()
        {
            return this.email;
        }

        internal string GetAddress()
        {
            return this.address;
        }


        internal void DeliverItem(Item Item , MemberRecord user )
        {
            //if( user.id.Equals( Item.getBuyer() ) )
               sellItemList.Add(Item);
        }

        internal void GainItem(Item Item , MemberRecord user )
        {
            //if( user.id.Equals( Item.getSeller() ) )
              buyItemList.Add(Item);
        }

        internal List<Item> GetSellItemList()
        {
            return sellItemList;
        }

        internal List<Item> GetBuyItemList()
        {
            return buyItemList;
        }

        internal void ReturnItem(Item Item, MemberRecord user)
        {
            for (int i = 0; i < buyItemList.Count; i++ ) 
            {
                if (Item.GetBuyer().Equals(name))
                    Item.SetBuyer("");
            }
            buyItemList.Remove(Item);
 
        }
    }
}
