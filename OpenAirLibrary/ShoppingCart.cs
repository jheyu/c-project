﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenAirLibrary
{
    class ShoppingCart
    {
        private List<Item> shoppingCart = new List<Item>();

        internal void Put(Item Item)
        {
            shoppingCart.Add(Item);
        }

        internal List<Item> Get()
        {
            return shoppingCart;
        }

        internal void Delete(Item Item)
        {
            shoppingCart.Remove(Item);
        }
    }
}
