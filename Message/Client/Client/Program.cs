﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Time_Client
{
    class Program
    {
        public static Socket _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        static void Main(string[] args)
        {
            try
            {
                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse("140.138.6.120"), 100);
                _clientSocket.Connect(ipep);
                //Console.WriteLine("Connection Established");
            }
            catch (SocketException)
            {
                Console.WriteLine("Connection Unestablished");
                LoopConnect();
            }
            string txt = ""; //初始化儲存指令及簡訊內容字串
            foreach (string s in args)//分割及儲存指令與訊息內容
            {
                txt = txt + " " + s;
            }
            txt = txt.TrimStart();//將字串前面的空格去除
            ProString(txt);//將字串傳送到ProString函數做分析及處理
            SendLoop(txt);//分析處理完畢的字串藉由SendLoop函數傳送到Server
            //System.Environment.Exit(1);
            //System.Environment.Exit(System.Environment.ExitCode);
            //_clientSocket.Close();
            //System.Environment.Exit(1);
        }
        //ProString函數,分析處理指令及簡訊內容字串
        private static void ProString(string text)
        {
            //光是指令就至少需要長度為四的字串
            if (text.Length < 4)
            {
                Console.WriteLine("Wrong order!!");
                Console.ReadLine();
            }
            //分析指令是否符合下列三個 ALL:全體 LIB:圖書館 BAS:地下室,正確就送出
            if (text.Substring(0, 4) == "ALL " || text.Substring(0, 4) == "LIB " ||
                  text.Substring(0, 4) == "BAS ")
            {
                return;
            }
            //指令也可能是電話號碼(不考慮+886的國際電話),分析是否為正確的手機號碼,正確就送出
            else if (text[0] == '0' && text[1] == '9')
            {
                //手機號碼為十碼
                if (text.Length < 10)
                {
                    Console.WriteLine("Wrong phone number!!");
                    Console.ReadLine();
                }
                //就算字串長度為十,也有可能是數字以外的字元,所以必須檢查每個字元
                for (int count = 0; count < 10; count++)
                {
                    //Console.WriteLine(count);
                    if (Char.IsNumber(text[count]))
                    {
                        if (count == 9)
                        {
                            return;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Wrong phone number!!");
                        Console.ReadLine();
                    }
                }
                return;
            }
            else
            {
                Console.WriteLine("Wrong order or wrong phone number!!");
                Console.ReadLine();
            }
        }
        //SendLoop函數,檢查字串完畢確定無誤,由這送至Server
        private static void SendLoop(string req)
        {

            byte[] buffer = Encoding.UTF8.GetBytes(req);
            _clientSocket.Send(buffer);
            byte[] receivedBuf = new byte[1024];
            int rec = _clientSocket.Receive(receivedBuf);
            byte[] data = new byte[rec];
            Array.Copy(receivedBuf, data, rec);
            Console.WriteLine("Received: " + Encoding.UTF8.GetString(data));
        }


        private static void LoopConnect()
        {
            int attempts = 0;

            while (!_clientSocket.Connected)
            {
                try
                {
                    attempts++;
                    _clientSocket.Connect(IPAddress.Loopback, 100);
                }
                catch (SocketException)
                {
                    Console.Clear();
                    Console.WriteLine("Connection attempts: " + attempts.ToString());

                    if (attempts >= 120)
                    {
                        System.Environment.Exit(System.Environment.ExitCode);
                    }
                }
            }
            Console.Clear();
            Console.WriteLine("Connected");
        }
    }
}
